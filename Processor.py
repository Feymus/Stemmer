# -*- coding: utf-8 -*-

'''
Created on Oct 22, 2017

@author: HP
'''

import os, re, json
import Stemmer, time

class Processor(object):
    
    ## Class Processor
    # 
    # Controls the documents processing to generate its dictionary


    ## Class constructor
    #
    # Initialize variables necessary for the process
    def __init__(self):
        self.words = {}
        self.terms = {}
        self.content = []
    
    ## Method read_dir
    #
    # Reads the documents from a directory and process all of them one by one
    # every term gets save in content for its later stemming
    #
    # @param directory where the documents are 
    def read_dir(self, directory):
        # Getting the name of the documents from the directoy
        files = [file for file in os.listdir(directory)
                       if os.path.join(directory, file)]
        files = sorted(files)
        
        self.content = []
        # Initializing a little regular expression to get the words
        term = r"[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ]+"
        
        for file_name in files:
            
            # Opening the file <file_name>
            with open(directory + '/' + file_name, 'r', encoding='utf-8') as file:
                
                file_content = ""
                file_content = file.read()
                
                # Getting its content and converting to lower case
                self.content = re.findall(term, file_content)
                self.content = [word.lower() for word in self.content]
                
                self.get_terms()
            
            file.close()
    
    ## Method get_terms
    #
    # Gets the root from each word and save it in terms along with
    # its variants, number of variants and total frequency to not 
    # make the stemming process to every word more than one time, the
    # words that have already been pass this gets save in words along
    # with its root term
    def get_terms(self):
        
        stemmer = Stemmer.Stemmer('spanish')
        
        for word in self.content:
            
            root = ""
            
            # If the word haven't already pass the stemming process
            if (word not in self.words):
                root = stemmer.stemWord(word)
                self.words[word] = root
            else:
                root = self.words[word]
            
            # If its the first time the root appear
            if (root not in self.terms):
                self.terms[root] = {'num_variants':1, 'variants':[word], 'total_frequency':1}
            else:
                if (word not in self.terms[root]['variants']):
                    self.terms[root]['variants'].append(word)
                    self.terms[root]['num_variants'] += 1
                self.terms[root]['total_frequency'] += 1

    ## Method save_terms
    #
    # This method save the terms, number of found terms and number of
    # distinct words found in a txt file
    def save_terms(self):
        
        final_output = ""
        
        for term in sorted(self.terms):
            tmp_out = (term + " - " + "número de variantes: "
                       "" + str(self.terms[term]['num_variants']) + ", variantes: "
                       "" + str(" ".join(self.terms[term]['variants'])) + ", Frecuencia total: "
                       "" + str(self.terms[term]['total_frequency']) + "\n")
            final_output += tmp_out
        with open('resultado.txt', 'w') as out:
            number_of_terms = len(self.terms)
            number_distinct_words = len(self.words)
            print("Terminos encontrados: " + str(number_of_terms) + "\n")
            print("Palabras distintas encontradas: " + str(number_distinct_words) + "\n")
            out.write("Terminos encontrados: " + str(number_of_terms) + "\n")
            out.write("Palabras distintas encontradas: " + str(number_distinct_words) + "\n")
            out.write(final_output)
        out.close()
        
        print("Diccionario guardado en 'resultado.txt'")
        
if __name__ == "__main__":
    start_time = time.time()
    p = Processor()
    p.read_dir("./files")
    print("--- %s seconds ---" % (time.time() - start_time))
    p.save_terms()